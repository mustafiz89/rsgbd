-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2015 at 08:44 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dslbd_rsgbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
`admin_id` int(3) NOT NULL,
  `admin_name` varchar(50) DEFAULT NULL,
  `admin_email` varchar(100) DEFAULT NULL,
  `admin_password` varchar(32) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`) VALUES
(1, 'admin', 'admin@admin.com', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advisory_committee`
--

CREATE TABLE IF NOT EXISTS `tbl_advisory_committee` (
`id` int(4) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_advisory_committee`
--

INSERT INTO `tbl_advisory_committee` (`id`, `title`, `description`) VALUES
(1, 'Management Officials', '<p>1. Mr. Md. Faruk Ahmed - Managing Director</p>\r\n\r\n<p>2. Maj S M Naimul Alam (Retd) - Director Operation</p>\r\n\r\n<p>3. Capt. Md. Zaman Sikdar(Retd) - AGM   Operation</p>\r\n\r\n<p>4. SWO Md. SelimHossain (Retd) - Sr.Co-ord (Con. Cell)</p>\r\n\r\n<p>5. Sgt  Md.  Jamal Uddin  (Retd) -   Manager Marketing</P>\r\n\r\n<p>6. Sgt Md. Moniruzzam  (Retd) - Manager Admin</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_board_member`
--

CREATE TABLE IF NOT EXISTS `tbl_board_member` (
`member_id` int(3) NOT NULL,
  `member_name` varchar(50) DEFAULT NULL,
  `designation` varchar(80) DEFAULT NULL,
  `f_name` varchar(50) DEFAULT NULL,
  `m_name` varchar(50) DEFAULT NULL,
  `home_town` varchar(50) DEFAULT NULL,
  `living_at` varchar(50) DEFAULT NULL,
  `educational_qualification` varchar(80) DEFAULT NULL,
  `business_portfolio` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_board_member`
--

INSERT INTO `tbl_board_member` (`member_id`, `member_name`, `designation`, `f_name`, `m_name`, `home_town`, `living_at`, `educational_qualification`, `business_portfolio`, `image`) VALUES
(1, 'ab', 'aa', 'ab', 'ab', 'ab', 'ab', 'ab', 'ab', 'images/board_member/user1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chairman_message`
--

CREATE TABLE IF NOT EXISTS `tbl_chairman_message` (
`chairman_id` int(3) NOT NULL,
  `chairman_name` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `chairman_short_message` text CHARACTER SET latin1,
  `chairman_long_message` text CHARACTER SET latin1,
  `image` varchar(50) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_chairman_message`
--

INSERT INTO `tbl_chairman_message` (`chairman_id`, `chairman_name`, `chairman_short_message`, `chairman_long_message`, `image`) VALUES
(1, ' Name', 'short message', ' Long message', 'images/chairman/Photo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE IF NOT EXISTS `tbl_gallery` (
`id` int(4) NOT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `image`) VALUES
(1, 'images/gallery/DSC05723.JPG'),
(2, 'images/gallery/DSC05724.JPG'),
(3, 'images/gallery/DSC05725.JPG'),
(4, 'images/gallery/DSC05728.JPG'),
(5, 'images/gallery/DSC05733.JPG'),
(6, 'images/gallery/DSC05737.JPG'),
(7, 'images/gallery/DSC05745.JPG'),
(8, 'images/gallery/DSC05748.JPG'),
(9, 'images/gallery/01.jpg'),
(10, 'images/gallery/02.jpg'),
(11, 'images/gallery/03.jpg'),
(12, 'images/gallery/04.jpg'),
(13, 'images/gallery/05.jpg'),
(14, 'images/gallery/06.jpg'),
(15, 'images/gallery/07.jpg'),
(16, 'images/gallery/08.jpg'),
(17, 'images/gallery/09.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_glance`
--

CREATE TABLE IF NOT EXISTS `tbl_glance` (
`id` int(3) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_glance`
--

INSERT INTO `tbl_glance` (`id`, `title`, `description`) VALUES
(1, 'Our Commitments', '<p>1. Our guards are trained, disciplined, obedient and polite.</p>\r\n<p>2. We don’t compromise with our responsibility.</p>\r\n<p>3. We are committed to earn your confidence on any given task.</p>\r\n<p>4. We pay due attention to any comments/advice from our clients.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE IF NOT EXISTS `tbl_service` (
`id` int(4) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`id`, `title`, `description`) VALUES
(1, 'Our Preferred Services', 'We offer following services to our valuable clients.\r\n\r\n<p>1.  We have the option to provide armed or unarmed guard as per your demand.</p>\r\n<p>2.     We provide all kind of transportation facility to carry your valuable goods and documents in any corner of the country.</p>\r\n<p>3.     We provide security guards for corporate office, industrial park as well as residential blocks.</p>\r\n<p>4. We provide security coverage for any temporary occasions like ceremonial gathering, traditional fair, trade fair, educational campaign, educational tour including tour guide, all kind of reunion, recreation centers, recreational tour etc.</p>\r\n<p>5.     We provide personal security guard as and when asked for.</p>\r\n\r\n<p>Our control cell works round the clock to collect the latest updates about our guards on duty,from the monitoring teams who are always on wheel working in their respective AOR (Area of Responsibility).Any negative comments on the performance of any guard are received from any clients before it was noticed by the monitoring team of that respective area are considered as a crime as per company policy.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_welcome_message`
--

CREATE TABLE IF NOT EXISTS `tbl_welcome_message` (
`welcome_id` int(3) NOT NULL,
  `welcome_title` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `welcome_description` text CHARACTER SET latin1
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_welcome_message`
--

INSERT INTO `tbl_welcome_message` (`welcome_id`, `welcome_title`, `welcome_description`) VALUES
(1, 'Welcome to RSG services Ltd', '‘Safety Comes First’. With this blood borne view of ensuring safety and security to man and material in any form and under any circumstances, a combined team of retired Army personals gave a thought to exercise their years of experience in a realistic ground. To materialize this idea in a coordinated way, they formulate a plan to come under a common and recognized shed. With a clear vision and strong commitment, on 2003 they step forward as ‘RSG Services Ltd’. Initially the company observed the security aspect of malty ferrous arena and judges the requirement of different sector as it suits.Then it started providing the service with a very careful way, where the client’s satisfaction was given the prime importance to perform any given task. For utmost dedication and sincerity this company listed themselves as a renowned security guard provider in the country within very short span of time and earned no of complementation from its clients. This company is always a step ahead in adapting latest technology to improve its service, which gave her an extra opportunity to gain full confidence of her clients.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_force`
--

CREATE TABLE IF NOT EXISTS `tbl_work_force` (
`id` int(3) NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_work_force`
--

INSERT INTO `tbl_work_force` (`id`, `title`, `description`) VALUES
(1, 'Our Commitments', '<p>1. Our guards are trained, disciplined, obedient and polite.</p>\r\n<p>2. We don’t compromise with our responsibility.</p>\r\n<p>3. We are committed to earn your confidence on any given task.</p>\r\n<p>4. We pay due attention to any comments/advice from our clients.</p>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
 ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_advisory_committee`
--
ALTER TABLE `tbl_advisory_committee`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_board_member`
--
ALTER TABLE `tbl_board_member`
 ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `tbl_chairman_message`
--
ALTER TABLE `tbl_chairman_message`
 ADD PRIMARY KEY (`chairman_id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_glance`
--
ALTER TABLE `tbl_glance`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_service`
--
ALTER TABLE `tbl_service`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
 ADD PRIMARY KEY (`welcome_id`);

--
-- Indexes for table `tbl_work_force`
--
ALTER TABLE `tbl_work_force`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
MODIFY `admin_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_advisory_committee`
--
ALTER TABLE `tbl_advisory_committee`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_board_member`
--
ALTER TABLE `tbl_board_member`
MODIFY `member_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_chairman_message`
--
ALTER TABLE `tbl_chairman_message`
MODIFY `chairman_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tbl_glance`
--
ALTER TABLE `tbl_glance`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_service`
--
ALTER TABLE `tbl_service`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_welcome_message`
--
ALTER TABLE `tbl_welcome_message`
MODIFY `welcome_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_work_force`
--
ALTER TABLE `tbl_work_force`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
