<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>RSG Services Ltd</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url()?>css/images/favicon.ico" />
        <link rel="stylesheet" href="<?php echo base_url()?>css/style.css" type="text/css" media="all" />
         <link rel="stylesheet" href="<?php echo base_url()?>css/custom.css" type="text/css" media="all" />
        <link rel="stylesheet" href="<?php echo base_url()?>css/flexslider.css" type="text/css" media="all" />

        <script src="<?php echo base_url()?>js/jquery-1.7.2.min.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
                <script src="js/modernizr.custom.js"></script>
        <![endif]-->
        <script src="<?php echo base_url()?>js/jquery.flexslider-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url()?>js/functions.js" type="text/javascript"></script>
    </head>
    <body>
        <!-- wrapper -->
        <div id="wrapper">
            <!-- shell -->
            <div class="shell">
                <!-- container -->
                <div class="container">

                    <!-- header -->
                    <header class="header">
                        <h1 id="logo"><a href="<?php echo base_url()?>welcome/index.aspx"></a></h1>
                        <nav id="navigation">
                            <ul>
                                <li><a href="<?php echo base_url();?>welcome/index.aspx">Home</a></li>
                                <li>
                                    <a href="#">Board Of Director<span></span></a>
                                    <ul>
                                        <li><a href="<?php echo base_url();?>welcome/chairman_message.aspx">Director's Message</a></li>
                                        <li><a href="<?php echo base_url();?>welcome/board_member.aspx">Board Member</a></li>

                                    </ul>
                                </li>

                                <li>
                                    <a href="#">Who We Are<span></span></a>
                                    <ul>
                                        <li><a href="<?php echo base_url();?>welcome/glance.aspx">At A Glance</a></li>
                                        <li><a href="<?php echo base_url();?>welcome/work_force.aspx">WorkForce</a></li>

                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url();?>welcome/service.aspx">Our Services</a></li>
                                <li><a href="<?php echo base_url();?>welcome/advisory_committee.aspx">Advisory Committee</a></li>
                                
                                
                                
                                <li><a href="<?php echo base_url();?>welcome/gallery.aspx">Gallery</a></li>
                                <li><a href="<?php echo base_url();?>welcome/contact.aspx">Contact</a></li>
                                
                               
                            </ul>
                        </nav>
                        <div class="cl">&nbsp;</div>
                    </header>
                    <!-- end of header -->

                    <div class="main">


                        
                        <?php echo $mid_content;?>






                        <!-- services -->
                        
                        <!-- end of services -->
                        <section class="services">
                            <div class="widget">
                               <h3>Contact Us</h3>
                                <ul>
                                    <li>Baitul Lodge, 183 Green Road,Dhanmondi, Dhaka.</li>
                                    <li><strong>Phone:</strong> 01942-367736, 01963-834263, 01917-728195 </li>
                                     <li><strong></strong>01938-673594, 01947-748060.</li>
                                       <li><strong></strong></li>
<!--                                    <li><strong>Email:</strong>template@proper.com</li>-->
                                </ul>
                            </div>
                            <div class="widget socials-widget">
                                <h3>Get Social</h3>
                                <p></p>
                                <a href="#" class="facebook-ico">facebook</a>
                                <a href="#" class="twitter-ico">twitter</a>
<!--                                <a href="#" class="rss-ico">rss</a>
                                <a href="#" class="in-ico">in</a>-->
                                <a href="#" class="skype-ico">skype</a>
                                <a href="#" class="google-ico">google</a>
                            </div>
                            <div class="cl">&nbsp;</div>
                        </section>
                    </div> 
                    <!-- end of main -->
                    
                </div>
                <!-- end of container -->
               
                
                    
               
                <div class="footer">
                    <nav class="footer-nav">
                        <ul>
                            <li><a style="color:black;"href="<?php echo base_url();?>welcome/index.aspx">Home</a></li>
                            <!--<li><a style="color:black;" href="#">About Us</a></li>-->
                            <li><a style="color:black;" href="<?php echo base_url();?>welcome/service.aspx">Services</a></li>                                                      
                            <li><a style="color:black;" href="<?php echo base_url();?>welcome/contact.aspx">Contact</a></li>
                           
                        </ul>
                    </nav>
                    <p style="color:black"class="copy">Copyright &copy; 2015 All Rights Reserved. Develop by <a href="http://dynamicsoftltd.com" target="_blank" >Dynamic software ltd</a> </p>
                </div>
                    
            </div>
            <!-- end of shell -->
        </div>
        <!-- end of wrappert -->
    </body>
</html>