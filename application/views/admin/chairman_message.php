<!--<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <?php //echo $title ?>
            <p style="color: green;" id="message">
                //<?php 
//                $msg=$this->session->userdata('message');
//                if($msg){
//                    echo $msg;
//                    $this->session->unset_userdata('message');
//                }
//                
//                
//                ?>
            </p>
        </h1>
    </div>
</div>-->

<div class="row">
    <div class="col-md-12 col-lg-12">
        <!--   Kitchen Sink -->
        <div class="panel panel-default">
            <div class="panel-heading">
              <h1 style="color:#898989">
            <?php echo $title ?>
            <p style="color: green;" id="message">
                <?php 
                $msg=$this->session->userdata('message');
                if($msg){
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                
                
                ?>
            </p>
        </h1>        
             </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                
                                <th>Name</th>
                                <th>Message</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                                                    
                            <tr>
                                
                                <td><?php echo $chairman_info->chairman_name;?></td>
                                <td><?php echo $chairman_info->chairman_short_message;?></td>
                                <td><img src="<?php echo base_url().$chairman_info->image;?>" width="100" height="100"></td>
                                <td>
                                    <a class="btn btn-primary" href="<?php echo base_url();?>administrator/chairman_message_edit/<?php echo $chairman_info->chairman_id;?>">
                                    <i class="fa fa-edit"></i>  
                                    Edit                                            
                                </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
    
</div>


