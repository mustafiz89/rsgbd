
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 style="color:#898989">
                    <?php echo $title ?>
                    <p style="color: green;" id="message">
                        <?php
                        $msg = $this->session->userdata('message1');
                        if ($msg) {
                            echo $msg;
                            $this->session->unset_userdata('message1');
                        }
                        ?>
                    </p>
                </h1>        
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form role="form" action="<?php echo base_url(); ?>administrator/save_gallery" method="post" id="form" enctype="multipart/form-data">

                            <div class="form-group">
                                <br><label class="col-lg-3 control-label">Gallery Image</label>
                                <div class="col-lg-9">
                                    <input type="file" name="image"  accept="image/*"><br>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" id="submit" class="btn btn-primary">Save Changes</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>     
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if (count($all_info) > 0) {
    ?>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <!--   Kitchen Sink -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 style="color:#898989">
                        View Information
                        <p style="color: green;" id="message">
                            <?php
                            $msg = $this->session->userdata('message');
                            if ($msg) {
                                echo $msg;
                                $this->session->unset_userdata('message');
                            }
                            ?>
                        </p>
                    </h1>        
                </div>
                <div class="panel-body">
                    <div class="table-responsive">

                       
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Serial</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($all_info as $v_info) {
                                    $i+=1;
                                    ?>                        
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><img src="<?php echo base_url() . $v_info->image ?>" width="100" height="100"></td>
                                        <td>
                                            <a class="btn btn-danger" href="<?php echo base_url(); ?>administrator/delete_gallery/<?php echo $v_info->id; ?>" onclick="return check_delete();">
                                                <i class="fa fa-trash-o"></i>  
                                                Delete                                        
                                            </a>
                                        </td>
                                    </tr>
        <?php
    }
    ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>

    </div>
    <?php
}
?>


