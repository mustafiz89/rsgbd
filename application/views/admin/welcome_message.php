<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <?php echo $title ?>
            <p style="color: green;" id="message">
                <?php 
                $msg=$this->session->userdata('message');
                if($msg){
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                
                
                ?>
            </p>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form role="form" action="<?php echo base_url();?>administrator/update_welcome_info" method="post" id="form">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Title</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="welcome_title" required value="<?php echo $welcome_info->welcome_title; ?>" ><br>
                                    <input type="hidden" class="form-control" id="welcome_id" name="welcome_id" required value="<?php echo $welcome_info->welcome_id; ?>" >

                                </div>     
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Description</label>
                                <div class="col-lg-9">
                                    <textarea class="form-control" rows="10" id="welcome_description" name="welcome_description" required><?php echo $welcome_info->welcome_description; ?></textarea><br>
                                </div>     
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" id="submit" class="btn btn-primary" onclick="update_data()">Update Changes</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>     
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<script>

   $('document').ready(function(){
       $('#submit').click(function(event){
           event.preventDefault();
           var x=$('#form').serializeArray();
           $.post(
                   $('#form').attr('action'),
                   x,
                   function(){
                       $('.class').html();
                   }
           )
       })
   })

</script>-->

<!--<script>
    
    //Create a boolean variable to check for a valid Internet Explorer instance.
    var xmlhttp = false;
//Check if we are using IE.
    try {
//If the Javascript version is greater than 5.
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
//alert ("You are using Microsoft Internet Explorer.");
    } catch (e) {
//If not, then use the older active x object.
        try {
//If we are using Internet Explorer.
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//alert ("You are using Microsoft Internet Explorer");
        } catch (E) {
//Else we must be using a non-IE browser.
            xmlhttp = false;
        }
    }
//If we are using a non-IE browser, create a javascript instance of the object.
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
//alert ("You are not using Microsoft Internet Explorer");
    }

    function update_data()
    {
        var welcome_id=document.getElementById("welcome_id").value;
        var welcome_title=document.getElementById("welcome_title").value;
        var welcome_description=document.getElementById("welcome_description").value;
        
        var queryString = "?welcome_id=" + welcome_id ;
        queryString +=  "&welcome_title=" + welcome_title + "&welcome_description=" + welcome_description;
        var serverpage = "<?php echo base_url(); ?>administrator/update_welcome_info"+queryString;

        //window.alert(serverpage);
        xmlhttp.open("GET", serverpage, true);
        xmlhttp.onreadystatechange = function()
        {
            //alert(xmlhttp.readystate);
            //alert(xmlhttp.status);

            if (xmlhttp.readyState === 4 && xmlhttp.status === 200)
            {
                var display=document.getElementById("message");
                display.innerHTML=xmlhttp.responseText;
            }
        }
        xmlhttp.send(null);
    }
    
 </script>-->