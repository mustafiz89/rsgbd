<div class="row">
    <div class="col-md-12">
        <h1 class="page-header">
            <?php echo $title ?>
            <p style="color: green;" id="message">
                <?php 
                $msg=$this->session->userdata('message');
                if($msg){
                    echo $msg;
                    $this->session->unset_userdata('message');
                }
                ?>
            </p>
        </h1>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-9">

                        <form role="form" action="<?php echo base_url(); ?>administrator/save_board_member" method="post" id="form" enctype="multipart/form-data">
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Name</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="member_name" required value=""><br>

                                </div>     
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Designation</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="designation" required value=""><br>

                                </div>     
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label">father's Name</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="f_name" required value=""><br>

                                </div>     
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Mother's Name</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="m_name" required value=""><br>

                                </div>     
                            </div>
                                                        
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Home Town</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="home_town" required value=""><br>

                                </div>     
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Living At(Settled)</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="living_at" required value=""><br>

                                </div>     
                            </div>
                             
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Educational Qualification</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="educational_qualification" required value=""><br>

                                </div>     
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Business Portfolio</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control" id="welcome_title" name="business_portfolio" required value=""><br>

                                </div>     
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Person Image</label>
                                <div class="col-lg-9">
                                    <input type="file" name="image" required accept="image/*"><br>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label"></label>
                                <div class="col-lg-9">
                                    <button type="submit" id="submit" class="btn btn-primary">Save Changes</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>     
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>